// Reste à gérer la deuxième partie de page qui disparaît quand on resize la page

let controller;

const setupScrollMagic = () => {
	controller = new ScrollMagic.Controller();

	let timeline = new TimelineMax();

	// Conditions basées sur la largeur de l'écran
	if (window.innerWidth >= 768) {
		// Pour les écrans larges (768 pixels et plus)
		timeline
			.to("#sixth", 6, { y: -700 })
			.to("#fifth", 6, { y: -500 }, "-=6")
			.to("#forth", 6, { y: -400 }, "-=6")
			.to("#third", 6, { y: -300 }, "-=6")
			.to("#second", 6, { y: -200 }, "-=6")
			.to("#first", 6, { y: -100 }, "-=6")
			.to(".content, .blur", 6, { top: "0%" }, "-=6")
			.to(".title, nav, .footer-wrapper", 6, { y: -600 }, "-=6")
			.from(".one", 3, { top: "40px", autoAlpha: 0 }, "-=4")
			.from(".two", 3, { top: "40px", autoAlpha: 0 }, "-=3.5")
			.from(".three", 3, { top: "40px", autoAlpha: 0 }, "-=3.5")
			.from(".four", 3, { top: "40px", autoAlpha: 0 }, "-=3.5")
			.from(".text", 3, { y: 60, autoAlpha: 0 }, "-=4");
	} else {
		// Pour les écrans étroits (moins de 768 pixels)
		timeline
			.to("#sixth", 4, { y: -400 })
			.to("#fifth", 4, { y: -300 }, "-=4")
			.to("#forth", 4, { y: -200 }, "-=4")
			.to("#third", 4, { y: -100 }, "-=4")
			.to("#second", 4, { y: -50 }, "-=4")
			.to("#first", 4, { y: -25 }, "-=4")
			.to(".content, .blur", 4, { top: "0%" }, "-=4")
			.to(".title, nav, .footer-wrapper", 4, { y: -300 }, "-=4")
			.from(".one", 2, { top: "20px", autoAlpha: 0 }, "-=2")
			.from(".two", 2, { top: "20px", autoAlpha: 0 }, "-=1.5")
			.from(".three", 2, { top: "20px", autoAlpha: 0 }, "-=1.5")
			.from(".four", 2, { top: "20px", autoAlpha: 0 }, "-=1.5")
			.from(".text", 2, { y: 30, autoAlpha: 0 }, "-=2");
	}

	let scene = new ScrollMagic.Scene({
		triggerElement: "section",
		duration: "200%",
		triggerHook: 0,
	})
		.setTween(timeline)
		.setPin("section")
		.addTo(controller);
};

document.addEventListener("DOMContentLoaded", () => {
	// Exécutez la configuration initiale
	setupScrollMagic();

	// Ajoutez un écouteur d'événement de redimensionnement de fenêtre
	window.addEventListener("resize", () => {
		// Détruit le contrôleur actuel et la scène
		controller.destroy(true);

		// Exécutez à nouveau la configuration avec les nouvelles conditions de taille d'écran
		setupScrollMagic();
	});

	// Remove 'no-js' class once the DOM is loaded and scripts executed
	document.documentElement.classList.remove("no-js");
});
